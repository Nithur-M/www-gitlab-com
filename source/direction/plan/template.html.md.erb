---
layout: markdown_page
title: Product Stage Direction - Plan
description: "The Plan stage enables teams to effectively plan features and projects in a single application"
canonical_path: "/direction/plan/"
---

Content last reviewed on 2023-08-07

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Enable teams to effectively plan and execute work in a single application</b>
    </font>
</p>

<%= partial("direction/plan/templates/overview") %>

<%= devops_diagram(["Plan"]) %>

## Stage Overview

The Plan Stage provides tools for teams to manage and optimize their work, track operational health and measure outcomes. As an end-to-end DevSecOps platform, GitLab is uniquely positioned to deliver a planning suite that enables business leaders to drive their vision and DevSecOps teams to deliver value while improving how they work. In addition, the unification of the DevSecOps process allows GitLab to interlink data across every stage of development, from ideation, to planning, implementation, deployment, and deliver value to customers.

### Group and Categories

<%= partial("direction/plan/templates/categories") %>

## 3 Year Stage Themes
<%= partial("direction/plan/templates/themes") %>
 
## 3 Year Strategy

In three years, the Plan Stage market will:
* Continue to shift from project to product and focus on outcomes instead of output.
* Continue to move away from command and control mentality and instead empower teams to determine how they can contribute toward business objectives.
* Make operational efficiency and continual improvement a top priority.  
* Embrace machine learning and automation within the Plan stage of the DevSecOps toolchain and lifecycle.
* Shift toward consolidation into a single platform for all stages of the DevSecOps lifecycle.

As a result, in three years, Gitlab will:
* Provide support for individual DevSecOps teams and entire organizations using scaled Agile frameworks.
* Allow GitLab to capture and tie metrics to [Work Items](https://docs.gitlab.com/ee/development/work_items.html) to reflect business outcomes. 
* Surface metrics like DORA and Value Stream in key parts of a teams workflow to help drive improvements. 
* Support frameworks like OKRs that encourage bottom-up contributions. 
* Use downstream DevSecOps data for automation and machine learning to help teams improve their plans.
* Make it easy for non-Developer Personas to contribute to, read, and edit data in GitLab. 

## 1 Year Plan

### What We Recently Completed

<%= partial("direction/plan/project_management/team_planning/recent_accomplishments") %>
* [Add or resolve a to do for work items](https://gitlab.com/groups/gitlab-org/-/epics/975)(**16.0**) - Added the ability to add or resolve a to do for work items as part of our effort to mature the work items framework so that it can support issues and epics in the near future. 
* [Auto-expanded summary](https://gitlab.com/gitlab-org/gitlab/-/issues/386937)(**15.10**) - Building off the success of [rendering a title](https://gitlab.com/gitlab-org/gitlab/-/issues/15694) when adding `+` to the end of a GitLab issue url, we are now introducing a summary version of this to display `title_assignee_milestone_health status`. This time saver is valuable when trying to convey a lot of information for little effort.
* [The GitLab Value Streams Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/9317)(**16.0**) - Enable decision-makers to identify trends, patterns, and opportunities for digital transformation improvements. 
* [Rich-text content editor](https://gitlab.com/groups/gitlab-org/-/epics/7098)(**16.2**) - We often hear feedback from non-developers that editing text in markdown is not their prefered experience. The new rich-text content editor imakes it easier for everyone to contribute by providing a real-time preview and live rich editing of Markdown content. 
* [Custom value streams for project-level value stream analytics](https://gitlab.com/gitlab-org/gitlab/-/issues/382496)(**15.11**) - To improve the visibility into the complete workstream, we are adding to the project-level Value Stream Analytics (VSA) the [Overview stage](https://docs.gitlab.com/ee/user/group/value_stream_analytics) and the option to [Custom value streams](https://docs.gitlab.com/ee/user/group/value_stream_analytics/#create-a-value-stream-with-custom-stages). Until now, these features were only available at the group-level VSA only.
* [Reorder records in work items hierarchy widgets](https://gitlab.com/groups/gitlab-org/-/epics/9548)(**16.0**) - As users add records to work item widgets like `tasks`, they may have a need to order the records in a specific way. With this work, we are creating a "drag and drop" feature to give users more flexibility.
* [Value Stream Dashboard Configuration](https://gitlab.com/gitlab-org/gitlab/-/issues/388890)(**16.2**) - With a configuration file for the new Value Stream Dashboard you can define various settings and parameters, such as title, description, and number of panels and label filters. You can adjust the metrics comparison panel based on your areas of interest, filter out irrelevant information, and focus on the data that is most relevant to your analysis or decision-making process.
* [Custom value streams for project-level value stream analytics](https://gitlab.com/gitlab-org/gitlab/-/issues/382496) - To improve the visibility into the complete workstream, we added to the project-level Value Stream Analytics (VSA) the Overview stage and the option to create custom value streams. Until now, these features were only available at the group-level VSA only.

### What We Are Currently Working On

* AI-Assisted Flows - The Plan stage is collaborating with [the AI-Assisted group](https://about.gitlab.com/handbook/product/categories/#ai-assisted-group) the to create experimental features that improve user productivity in planning workflows. Examples include:
   * [Summarizing issue comments](https://about.gitlab.com/blog/2023/04/13/summarize-issues/)
   * [Creating issue descriptions](https://gitlab.com/gitlab-org/gitlab/-/issues/406791)
* Migrating [epics to the work items](https://gitlab.com/groups/gitlab-org/-/epics/9290) framework will allow us to eventually bring more consistency with issues and address long-standing requests, like [assignees on epics](https://gitlab.com/groups/gitlab-org/-/epics/4231).
<%= partial("direction/plan/project_management/team_planning/current_focus") %>
* Improving performance will continue to be a top priority in the near term. The rapid growth of GitLab.com has uncovered the need for continue focus on [database and query optimization](https://gitlab.com/groups/gitlab-org/-/epics/5804).
* [Pages Multi-Version Support](https://gitlab.com/groups/gitlab-org/-/epics/10914) - Currently a project can have only a single version of a GitLab Pages site. This make it hard for customers to try new ideas on their sites without changing the only version of the site. Customers need a way to preview changes or have multiple environments for their GitLab Pages sites to make it possible to validate changes before deploying their site.
* [DORA Performance Panel](https://gitlab.com/groups/gitlab-org/-/epics/10416) - The Value Streams Dashboard can now be used to visualize the status of the organization's DevOps performance levels across different projects. This new view will enable the executive to track how the organization implements DevOps practices, and compare the DevOps DORA score performance against industry benchmarks.

<%= partial("direction/plan/templates/next") %>

### Cross-Stage Initiatives

Plan offers functionality that ties into workflows in other stages.  We are actively collaborating with other stages that are building upon Plan functionality to meet their users needs.

* The Manage:Import and Integrate group has built a Jira integration that displays Jira Issue data within GitLab. We will collaborate with that team to tie Jira Issues into more workflows like reporting and tying Jira Issues to higher level work items.
* The Manage:Import and Integrate owns the [Jira importer](https://docs.gitlab.com/ee/user/project/import/jira.html) to allow Jira issues to be migrated to GitLab. We will continue to work with that team to extend GitLab work items to accomodate more critical data elements from Jira to ensure a seamless import process.
* The Plan:Project Management and Create:Editor groups will work together to incroporate the new WSWYG markdown editor into Work Items.
* The Plan and Manage stages are collaborating on the [Organizations](https://gitlab.com/groups/gitlab-org/-/epics/6473) initiative to simplify the management of work and team hierarchies in GitLab.
* The Monitor:Respond group built Incidents based on Issues with guidance from the Plan:Project Management group. We will continue to collaborate as they [migrate Incidents to our new Work Item architecture](https://gitlab.com/groups/gitlab-org/-/epics/9585).
* Plan:Project Management, Govern:Compliance, Verify, and Release are working together on CloudEvents and Eventing as part of Workflows & Automations validation (gitlab-org/gitlab#344136).
* The Plan stage, Monitor:Respond group, and the [Service Desk Single Engineer Group](https://about.gitlab.com/handbook/engineering/incubation/service-desk/) are collaborating on [accelerating Service Desk](https://gitlab.com/groups/gitlab-org/-/epics/8769), which will extend work items to support adjacent use cases to portfolio and team planning. 
* Plan:Project Management is collaborating with the [Real-time Editing (REID) Single Engineer Group](https://about.gitlab.com/handbook/engineering/incubation/real-time-collaboration/) to bring real-time, conflict-free text editing to work items. 
* Plan:Project Management is collaborating with the [No-code/Low-code Single Engineer Group](https://about.gitlab.com/handbook/engineering/incubation/no-code/) to launch a native automation framework targeting non-developer personas. 
* Plan:Product Planning is collaborating with the [OKR Single Engineer Group](https://about.gitlab.com/handbook/engineering/incubation/okr/) to make OKRs generally available within GitLab.
* Plan:Optimize is collaborating with Manage:organization to Consolidate [Value Stream Analytics Group & Project into a single object - Workspace](https://gitlab.com/groups/gitlab-org/-/epics/9295). 
* Plan:Optimize is collaborating with Analytics:Product Analytics to add [YML schema-driven customizable UI](https://gitlab.com/groups/gitlab-org/-/epics/8925) to the Value Streams Dashboard. 
* Plan:Optimize is collaborating with Govern:Threat Insights to add [Vulnerabilities metrics to the Value Streams Dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/383697).
Please explore the individual [Category](https://about.gitlab.com/direction/plan/#categories) Direction pages for more information on 12 month plans.

### Target audience

<%= partial("direction/plan/templates/target_audience") %>

### Pricing

<%= partial("direction/plan/templates/pricing") %>

An example of what the end result data model and pricing could look like based on these pricing principles:

![Work Items Hierarchy](/images/direction/plan/workitemhierarchy.png)


### Jobs To Be Done 

[View the Plan stage JTBD](/direction/plan/jtbd.html)
